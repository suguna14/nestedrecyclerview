package com.listofcompanies;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit.ListofCompaniesModel;
import retrofit.ListofCompaniesModel.Details;


/**
 * Created by Suguna on 11/20/16.
 */

public class SubItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Details> subItemsList = new ArrayList();
    private Details details;

    public SubItemsAdapter(Context context, List<Details> subItemsList) {
        super();
        this.subItemsList = subItemsList;
    }

    @Override
    public int getItemCount() {
        return subItemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {

        if (holder instanceof MenuViewHolder) {

            details = subItemsList.get(i);
            ((MenuViewHolder) holder).ceoName.setText(details.getCeoName());
            ((MenuViewHolder) holder).type.setText(details.getOwned());
            ((MenuViewHolder) holder).streetAddress.setText(details.getStreetAddress());
            ((MenuViewHolder) holder).location.setText(details.getLocation());
        }
    }

    /* (non-Javadoc)
      * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
      */
    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int position) {

        View row = null;
        row = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_rows_layout, parent, false);
        MenuViewHolder vh = new MenuViewHolder(row);
        return vh;

    }


    public class MenuViewHolder extends RecyclerView.ViewHolder {

        protected TextView ceoName, type, streetAddress, location;
        protected RecyclerView recyclerView;

        public MenuViewHolder(View v) {
            super(v);
            ceoName = (TextView) v.findViewById(R.id.ceoName);
            type = (TextView) v.findViewById(R.id.type);
            streetAddress = (TextView) v.findViewById(R.id.streetAddress);
            location = (TextView) v.findViewById(R.id.location);
        }
    }

}
