package com.listofcompanies;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.ListofCompaniesModel;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RetrofitInerface;

public class MainActivity extends AppCompatActivity {

    private RecyclerView headerRecyclerView;
    private TextView headerTitle;
    private ArrayList<Header> data;
    private static final String BASE_URL = "https://gist.github.com";
    private HeaderRowAdapter headerRowAdapter;
    private Context m_context;
    private Handler mainHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainHandler = new Handler(Looper.getMainLooper());


        headerTitle = (TextView) findViewById(R.id.titleName);
        headerRecyclerView = (RecyclerView) findViewById(R.id.headerRecyclerView);
        headerTitle.setText(this.getResources().getString(R.string.headerName));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setAutoMeasureEnabled(true);
        headerRecyclerView.setLayoutManager(linearLayoutManager);
        headerRecyclerView.setHasFixedSize(false);
        headerRecyclerView.setNestedScrollingEnabled(true);
        headerRecyclerView.setHorizontalScrollBarEnabled(true);
        m_context = this;

        getTheData();

    }

    /**
     * Getting the data from the retrofit service call....
     */

    public void getTheData() {

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build();

        RetrofitInerface service = retrofit.create(RetrofitInerface.class);

        Call<ListofCompaniesModel> call = service.getCompaniesList();

        call.enqueue(new Callback<ListofCompaniesModel>() {
            @Override
            public void onResponse(Response<ListofCompaniesModel> response, Retrofit retrofit) {
                ListofCompaniesModel result = response.body();
                fillTheRecyclerView(result);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("Service Call Failed:", t.toString());
            }
        });
    }

    public void fillTheRecyclerView(final ListofCompaniesModel result){

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                headerRowAdapter = new HeaderRowAdapter(m_context, result.getCompanies());
                headerRecyclerView.setAdapter(headerRowAdapter);
            }
        };
        mainHandler.post(runnable);

    }
}
