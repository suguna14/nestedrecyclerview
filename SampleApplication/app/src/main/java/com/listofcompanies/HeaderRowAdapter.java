package com.listofcompanies;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit.ListofCompaniesModel;
import retrofit.ListofCompaniesModel.Company;


/**
 * Created by Suguna on 11/20/16.
 */

public class HeaderRowAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Company> headerGroupArrayList;
    private Company header;
    private Context m_context;

    public HeaderRowAdapter(Context context, List<Company> headerGroupArrayList) {
        super();
        this.headerGroupArrayList = (ArrayList<Company>) headerGroupArrayList;
        this.m_context = context;
    }


    @Override
    public int getItemCount() {
        return headerGroupArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int i) {

        if (holder instanceof MenuViewHolder) {

            header = headerGroupArrayList.get(i);

            ((MenuViewHolder) holder).companyName.setText(header.getName());

           if (header.getDetails() != null) {

                SubItemsAdapter subMenuAdapter = new SubItemsAdapter(m_context, (List<ListofCompaniesModel.Details>) header.getDetails());
                ((MenuViewHolder) holder).recyclerView.setAdapter(subMenuAdapter);

            }
        }

    }


    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View row = null;
        row = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_rows_layout, parent, false);
        return new MenuViewHolder(row);
    }


    public class MenuViewHolder extends RecyclerView.ViewHolder {
        protected TextView companyName;
        protected RecyclerView recyclerView;

        public MenuViewHolder(View v) {
            super(v);
            recyclerView = (RecyclerView) v.findViewById(R.id.headerRecyclerView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(v.getContext(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(true);

            companyName = (TextView) v.findViewById(R.id.companyName);


        }
    }
}

