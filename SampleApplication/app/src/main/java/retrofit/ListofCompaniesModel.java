package retrofit;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 *
 * {
 "Companies": [{
 "name": "AAA Company",
 "details": {
 "ceoName": "Mr. David",
 "owned": "family owned",
 "streetAddress": "100, Princeton Road",

 "location": "New Jersey"
 }
 }, {
 "name": "BBB Company",
 "details": {
 "ceoName": "Mr. Joseph",
 "owned": "public corporation",
 "streetAddress": "10, Washington Road",

 "location": "MaryLand"
 }
 }, {
 "name": "CCC Company",
 "details": {
 "ceoName": "Mr. Wallace",
 "owned": "public corporation",
 "streetAddress": "14, Jackson Road",

 "location": "Florida"
 }
 }, {
 "name": "DDD Company",
 "details": {
 "ceoName": "Ms. Olivia",
 "owned": "public corporation",
 "streetAddress": "1, Washington Road",

 "location": "Delaware"
 }
 }
 ]
 }
 */

public class ListofCompaniesModel {

    @SerializedName("Companies")
    @Expose
    private List<Company> companies = null;

    /**
     *
     * @return
     * The companies
     */
    public List<Company> getCompanies() {
        return companies;
    }

    /**
     *
     * @param companies
     * The Companies
     */
    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }



public class Company {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("details")
    @Expose
    private List<Details> details;

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The details
     */
    public List<Details> getDetails() {
        return details;
    }

    /**
     *
     * @param details
     * The details
     */
    public void setDetails(List<Details> details) {
        this.details = details;
    }

}
public class Details {

    @SerializedName("ceoName")
    @Expose
    private String ceoName;
    @SerializedName("owned")
    @Expose
    private String owned;
    @SerializedName("streetAddress")
    @Expose
    private String streetAddress;
    @SerializedName("location")
    @Expose
    private String location;

    /**
     *
     * @return
     * The ceoName
     */
    public String getCeoName() {
        return ceoName;
    }

    /**
     *
     * @param ceoName
     * The ceoName
     */
    public void setCeoName(String ceoName) {
        this.ceoName = ceoName;
    }

    /**
     *
     * @return
     * The owned
     */
    public String getOwned() {
        return owned;
    }

    /**
     *
     * @param owned
     * The owned
     */
    public void setOwned(String owned) {
        this.owned = owned;
    }

    /**
     *
     * @return
     * The streetAddress
     */
    public String getStreetAddress() {
        return streetAddress;
    }

    /**
     *
     * @param streetAddress
     * The streetAddress
     */
    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    /**
     *
     * @return
     * The location
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

}


}
